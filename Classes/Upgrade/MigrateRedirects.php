<?php
declare(strict_types = 1);
namespace Jigal\MigrateRedirects\Upgrade;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Install\Updates\AbstractDownloadExtensionUpdate;
use TYPO3\CMS\Install\Updates\Confirmation;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\ExtensionModel;
use TYPO3\CMS\Core\Utility\MathUtility;

class MigrateRedirects extends AbstractDownloadExtensionUpdate
{

    /** @var array  */
    protected $tableNames = [
        'tx_myredirects_domain_model_redirect' => 'url',
        'tx_realurl_redirects' => 'url',
    ];

    /**
     * Little cache to save lookups while migrating data
     *
     * @var array
     */
    protected $domainRecordCache = [];

    /**
     * @var \TYPO3\CMS\Install\Updates\Confirmation
     */
    protected $confirmation;

    public function __construct()
    {
        $this->extension = new ExtensionModel(
            'redirects',
            'Redirects',
            '9.5',
            'typo3/cms-redirects',
            'Manage redirects for your TYPO3-based website'
        );

        $this->confirmation = new Confirmation(
            'Are you sure?',
            'You should install the "redirects" extension only if needed. ' . $this->extension->getDescription(),
            true
        );
    }

    /**
     * Return a confirmation message instance
     *
     * @return \TYPO3\CMS\Install\Updates\Confirmation
     */
    public function getConfirmation(): Confirmation
    {
        return $this->confirmation;
    }


    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'migrateRedirects';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Migrate redirects from EXT:realurl and EXT:my_redirects to core redirects';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'The extensions RealUrl(1.x) and MyRedirects provided a module to redirect URLs to other'
            . ' URLs. This functionality is now included in the core of TYPO3 9LTS. The records of both'
            . ' extensions can be migrated to core redirects to make upgrading to TYPO3 v9 easier. The'
            . ' system extension "Redirects" will be installed and the database tables "sys_domain" and'
            . ' one of "tx_myredirects_domain_model_redirect", "tx_realurl_redirects" must be present.';
    }

    /**
     * Performs the update:
     * - Install EXT:redirects
     * - Migrate DB records
     *
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     * @throws \TYPO3\CMS\Extensionmanager\Exception\ExtensionManagerException
     */
    public function executeUpdate(): bool
    {
        // Install the EXT:redirects extension if not happened yet
        $installationSuccessful = $this->installExtension($this->extension);
        if ($installationSuccessful) {
            // Migrate the database entries
            $this->migrateRedirectsToSysRedirect();
        }
        return $installationSuccessful;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function updateNecessary(): bool
    {
        $updateNeeded = false;
        // Check if the database table exists
        // and if either source table is present and contains records
        if ($this->checkIfWizardIsRequired()) {
            $updateNeeded = true;
        }
        return $updateNeeded;
    }

    /**
     * Returns an array of class names of Prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class
        ];
    }

    /**
     * Checks if either table is present and not empty
     *
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function checkIfWizardIsRequired(): bool
    {
        if (!$this->checkIfTableIsPresentAndNotEmpty('sys_domain', 'domainName')) {
            return false;
        }
        foreach ($this->tableNames as $tableName => $checkField) {
            if ($this->checkIfTableIsPresentAndNotEmpty($tableName, $checkField)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $tableName
     * @param string $checkField
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function checkIfTableIsPresentAndNotEmpty(string $tableName, string $checkField): bool
    {
        $tablePresent = false;
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $connection = $connectionPool->getConnectionByName('Default');
        $columns = $connection->getSchemaManager()->listTableColumns($tableName);
        if (isset($columns[strtolower($checkField)])) {
            // Table is present
            $connection = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable($tableName);
            $numberOfEntries = $connection->count('*', $tableName, []);
            if ($numberOfEntries > 0) {
                $tablePresent = true;
            };
        }
        return $tablePresent;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    protected function migrateRedirectsToSysRedirect(): void
    {
        foreach ($this->tableNames as $tableName => $checkField) {
            if ($this->checkIfTableIsPresentAndNotEmpty($tableName, $checkField)) {
                switch ($tableName) {
                    case 'tx_myredirects_domain_model_redirect':
                        $this->migrateMyRedirects();
                        break;
                    case 'tx_realurl_redirects':
                        $this->migrateRealurlRedirects();
                        break;
                    default:
                        throw new \Exception('Table "' . $tableName . '"" for migration of redirects not supported', 1538808527);
                }
            }
        }
    }

    protected function migrateMyRedirects(): void
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_myredirects_domain_model_redirect');
        $targetConnection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('sys_redirects');
        $redirects = $connection->select(['*'], 'tx_myredirects_domain_model_redirect');
        foreach ($redirects->fetchAll() as $redirect) {
            $newRedirect = [
                'updatedon' => (int)$redirect['tstamp'],
                'createdon' => (int)$redirect['crdate'],
                'deleted' => 0,
                'disabled' => 1 - (int)$redirect['active'],
                'starttime' => 0,
                'endtime' => 0,
                'source_host' => $this->getDomain($redirect['domain']),
                'source_path' => $redirect['url'],
                'is_regexp' => 0,
                'force_https' => 0,
                'keep_query_parameters' => 0,
                'target' => $redirect['destination'],
                'target_statuscode' => $redirect['http_response'],
                'hitcount' => (int)$redirect['counter'],
                'lasthiton' => (int)$redirect['last_hit'],
                'disable_hitcount' => 0,
            ];
            $targetConnection->insert('sys_redirect',$newRedirect);
        }
    }

    protected function migrateRealurlRedirects(): void
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_realurl_redirects');
        $targetConnection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('sys_redirects');
        $redirects = $connection->select(['*'], 'tx_realurl_redirects');
        foreach ($redirects->fetchAll() as $redirect) {
            $newRedirect = [
                'updatedon' => (int)$redirect['tstamp'],
                'createdon' => (int)$redirect['tstamp'],
                'deleted' => 0,
                'disabled' => 0,
                'starttime' => 0,
                'endtime' => 0,
                'source_host' => $this->getDomain($redirect['domain_limit']),
                'source_path' => $this->correctUrl($redirect['url']),
                'is_regexp' => 0,
                'force_https' => 0,
                'keep_query_parameters' => 0,
                'target' => $redirect['destination'],
                'target_statuscode' => $redirect['has_moved'] ? 301 : 302,
                'hitcount' => (int)$redirect['counter'],
                'lasthiton' => 0,
                'disable_hitcount' => 0,
            ];
            $targetConnection->insert('sys_redirect',$newRedirect);
        }
    }

    /**
     * @param int $domain
     * @return string
     */
    protected function getDomain(int $domain): string
    {
        if (!isset($this->domainRecordCache[$domain])) {
            if ($domain === 0) {
                $domainName = '*';
            } else {
                $connection = GeneralUtility::makeInstance(ConnectionPool::class)
                    ->getConnectionForTable('sys_domain');
                $domainName = $connection->select(['domainName'], 'sys_domain', ['uid' => $domain])
                    ->fetch(\PDO::FETCH_ASSOC);
            }
            $this->domainRecordCache[$domain] = $domainName['domainName'];
        }
        return $this->domainRecordCache[$domain];
    }

    /**
     * Borrowed from the MyRedirects convert script
     * Correct url based on RealURL configuration
     * if defaultToHTMLsuffixOnPrev is not set, force the trailing / in url
     *
     * @param string $url
     * @return string
     */
    protected function correctUrl(string $url): string
    {
        $urlParameters = parse_url($url);
        if (
            isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'], $urlParameters['path'])
            && substr($url, -1) !== '/'
            && ExtensionManagementUtility::isLoaded('realurl')
            && (int)$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['fileName']['defaultToHTMLsuffixOnPrev'] === 0
        ) {
            $pathInfo = pathinfo($urlParameters['path']);
            if (empty($pathInfo['extension'])) {
                $urlParameters['path'] = rtrim($urlParameters['path'], '/') . '/';
                $url = HttpUtility::buildUrl($urlParameters);
            }
        }
        // Only look if query is configured and link is relative to the root
        if (isset($urlParameters['query']) && !isset($urlParameters['host'])) {
            $idOnlyRegEx = '/^id=[1-9][\d]{0,15}$/i';
            if (preg_match($idOnlyRegEx, $urlParameters['query'])) {
                $pageId = (int)str_replace('id=', '', $urlParameters['query']);
                if ($pageId > 0) {
                    $url = 't3://page?uid=' . $pageId;
                }
            } elseif (MathUtility::canBeInterpretedAsInteger($urlParameters['query'])) {
                $url = 't3://page?uid=' . $urlParameters['query'];
            }
        }
        return $url;
    }
}